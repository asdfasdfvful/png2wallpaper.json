import java.io.*;
import java.util.*;

public class Wall_Json
{
    private static File[] images;
    private static String dir;
    private static StringBuilder json;
    
    public static void main(String args[])
    {
        // Get .jar directory
        dir = new File(Wall_Json.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent();
        // Replace %20 with real spaces
        dir = dir.replace("%20", " ");
        
        // Show progress & Scan
        System.out.println("\nDirectory: " + dir + "\nScanning...");
        images = Scan4Images();
        
        // Write images if found, else return error message
        if (images.length > 0)
        {
            System.out.println("Found " + images.length + " images");
            
            writeXML();
            saveFiles();
            System.out.print("Finished!");
        }
        else
            System.out.print("Could not find any .png image files. :(");
    }

    private static File[] Scan4Images()
    {
        File directory = new File(dir);
        
        return directory.listFiles(new FilenameFilter()
        { 
             public boolean accept(File dir, String filename)
             {
                 return filename.endsWith(".png") || filename.endsWith(".PNG");
             }
        } );
    }
    
    private static void writeXML()
    {
        // Initialize String
        json = new StringBuilder();
        
        // Write headers
        json.append("{\n");
        json.append("\"wallpapers\":\n");
        json.append("        [\n");

        
        // Loop through all files and assign a value for each
        //for(File img : images)
        for (int i=0; i<images.length; i++)
        {
            json.append("                {\n");
            json.append("                        \"" + "author\":" + " \"Pitched Apps\",\n");   
            json.append("                        \"" + "url\":" + " \"https://raw.githubusercontent.com/asdfasdfvful/Pitched-Wallpapers/master/Material_Glass/" + images[i].getName() + "\",\n");
            json.append("                        \"" + "name\":" + " \"" + images[i].getName().replace(".png", "").replace(".PNG", "") +   "\"\n");
            //json.append("                \"" + "file_size\":" + " " + images[i].length() + "\n"); 
            //json.append("        },\n");
            if (i == images.length-1) {
                json.append("                }\n");
            } else {
                json.append("                },\n");
            }
        }
        json.append("\n");
        json.append("        ]\n");
        json.append("}");
    }
    
    private static void saveFiles()
    {
        // Save json.xml
        try
        {
            File jsonFile = new File(dir, "0wallpapers.json");
            FileOutputStream f = new FileOutputStream(jsonFile);
            f.write(json.toString().getBytes());
            f.close();
        }
        catch(Exception e)
        {
            System.out.println("Oops, an error occured while writing 0wallpapers.json");
        }
    }
}
