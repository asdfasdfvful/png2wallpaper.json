PNG2Wallpaper.json

Easily create a wallpaper.json from png images in a folder.

Instructions

1. Download the jar file
2. Put it in the folder with all the images (this only works with png files)
3. Run the program

Make sure you only have the main backgrounds in the directory. Move the thumbnail images somewhere else!
The second option is to make jpg thumbnails and to change the "_thumb.png" to "_thumb.jpg" in the java file and recreate the jar.

Also make sure your thumbnail is named like the main background plus "_thumb"

If you'd like to make any modifications, just modify the java file.

A good program for this is BlueJ: http://www.bluej.org/

Thanks to Pkmmte and Ivon for the help!